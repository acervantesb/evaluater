﻿using System.ComponentModel;

namespace EvaluateR.Data.Residentes
{
    public enum EspecialidadesEnum
    {
        [Description("Medicina Familiar y Comunitaria")]
        MedicinaFamiliarComunitaria,
        [Description("Medicina Intensiva")]
        MedicinaIntensiva,
        [Description("Medicina Interna")]
        MedicinaInterna,
        [Description("Cardiología")]
        Cardiologia,
        [Description("Cirugía General")]
        CirugiaGeneral,
        [Description("Cirugía Ortopédica y Traumatología")]
        Traumatologia,
        [Description("Oncología Médica")]
        OncologiaMedica,
        [Description("Oncología Radioterápica")]
        OncologiaRadioterapica,
        [Description("Radiodiagnóstico")]
        Radiodiagnostico,
        [Description("Medicina Preventiva")]
        MedicinaPreventiva,
        [Description("Psiquiatría")]
        Psiquiatria,
        [Description("Neumología")]
        Neumologia

    }
}
